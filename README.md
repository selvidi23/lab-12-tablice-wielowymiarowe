// Proszę napisać program, który będzie umożliwiał przechowywanie liczb całkowitych w tablicy dwuwymiarowej, o wymiarach 5x5 i pozwalał na wykonanie następujących operacji:
// wypełnienie całej tablicy podaną liczbą,
// wyświetlenie całej tablicy (dwuwymiarowo),
// wprowadzenie pojedynczej liczby na podanej pozycji,
// wyświetlenie pojedynczej liczby z podanej pozycji,
// wypełnienie wybranego wiersza kolejnymi liczbami, zaczynając od podanej,
// wypełnienie wybranej kolumny kolejnymi liczbami, zaczynając od podanej,
// obliczenie sumy liczb w wybranym wierszu,
// obliczenie sumy liczb w wybranej kolumnie,
// obliczenie sumy liczb w całej tablicy,
// obliczenie sumy liczb na przekątnych (dwie oddzielne sumy),
// wyświetlenie wybranego fragmentu tablicy.

