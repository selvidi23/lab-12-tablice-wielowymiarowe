#include <stdio.h>
// Proszę napisać program, który będzie umożliwiał przechowywanie liczb całkowitych w tablicy dwuwymiarowej, o wymiarach 5x5 i pozwalał na wykonanie następujących operacji:
// wypełnienie całej tablicy podaną liczbą,
// wyświetlenie całej tablicy (dwuwymiarowo),
// wprowadzenie pojedynczej liczby na podanej pozycji,
// wyświetlenie pojedynczej liczby z podanej pozycji,
// wypełnienie wybranego wiersza kolejnymi liczbami, zaczynając od podanej,
// wypełnienie wybranej kolumny kolejnymi liczbami, zaczynając od podanej,
// obliczenie sumy liczb w wybranym wierszu,
// obliczenie sumy liczb w wybranej kolumnie,
// obliczenie sumy liczb w całej tablicy,
// obliczenie sumy liczb na przekątnych (dwie oddzielne sumy),
// wyświetlenie wybranego fragmentu tablicy.

int array[5][5];

void fill_Array(int x,int y,int number){
        x=x-1;
        y=y-1;
        for(int i=x;i<5;i++){
                for(int j=y;j<5;j++){
                        array[i][j]=number;
                }
        }
}

void print_array(int x, int y){
        x=x-1;
        y=y-1;
        for(int i = x;i<5;i++){
                for(int j=y;j<5;j++){
                        printf("%d ",array[i][j]);
                }
                printf("\n");
                
        }
}

void change_Value_On_Index(int x,int y, int value){
        x=x-1;
        y=y-1;
        array[x][y] = value;
}

void print_Value_From_Index(int x,int y){
        x=x-1;
        y=y-1;
        printf("Index x = %d y = %d wartosc = %d",x+1,y+1,array[x][y]);
}

int sum_Value_Array(int x,int y,int flag){
        int result =0;
        x=x-1;
        y=y-1;
        if(flag ==0){
                for(int i=0;i<5;i++){
                        result=result+array[x][i];
                }
                return result;
        }else if(flag == 1){
                for(int i=0;i<5;i++){
                        result=result+array[i][y];
                }
                return result;
        }else if (flag ==2){
                for(int i = 0;i<5;i++){
                        for(int j=0;j<5;j++){
                                result=result+array[i][j];
                        }
                        
                
                }
                return result;
        }
        
       
        return result;
}

void sum_Diagonals(){
        int result=0;
        int result2=0;
        int j=4;
        for(int i = 0;i<5;i++){
                result = result+array[i][i];
                result2 = result2 + array[i][j];
                j=j-1;       
        }
        printf("Suma 1 przekątnej wynosi %d 2 przekątnej wynosi %d \n",result,result2);
}

void fill_Row(int x,int number){
        x=x-1;
        for(int i=0;i<5;i++){
                array[x][i]=number;
                number++;
        }
}

void fill_Column(int x,int number){
        x=x-1;
        for(int i=0;i<5;i++){
                array[i][x]=number;
                number++;
        }
}

int menu(){
        int liczba;
        printf("\n");
        printf("Wybierz opcje \n");
        printf(" \n");

        printf("1. Wypełnienie całej tablicy podaną liczbą \n");
        printf("2. Wyświetlenie całej tablicy (dwuwymiarowo) \n");
        printf("3. Wprowadzenie pojedynczej liczby na podanej pozycji\n");
        printf("4. Wyświetlenie pojedynczej liczby z podanej pozycji,\n");
        printf("5. Wypełnienie wybranego wiersza kolejnymi liczbami, zaczynając od podanej, \n");
        printf("6. Wypełnienie wybranej kolumny kolejnymi liczbami, zaczynając od podanej, \n");//poprawa
        printf("7. Obliczenie sumy liczb w wybranym wierszu, \n");
        printf("8. Obliczenie sumy liczb w wybranej kolumnie, \n");
        printf("9. Obliczenie sumy liczb w całej tablicy, \n");
        printf("10. Obliczenie sumy liczb na przekątnych (dwie oddzielne sumy), \n");
        printf("11. Wyświetlenie wybranego fragmentu tablicy. \n");
        printf("0. Koniec Programu \n");
        printf(" \n");

        scanf("%d",&liczba);

        return liczba;
}

int main(){
        int a,x,y,select;
        char letter;
        do{
                select = menu();
                switch (select)
                {
                        case 1:
                                printf("Podaj liczbe ktora chcesz wypelnic tablice \n");
                                scanf("%d",&a);
                                fill_Array(1,1,a);
                        break;
                        case 2:
                                print_array(1,1);
                        break;
                        case 3:
                                printf("Podaj index wiersza \n");
                                scanf("%d",&x);
                                printf("Podaj index kolumny \n");
                                scanf("%d",&y);
                                printf("Podaj Wartosc \n");
                                scanf("%d",&a);
                                change_Value_On_Index(x,y,a);
                        break;
                        case 4:
                                printf("Podaj index wiersza \n");
                                scanf("%d",&x);
                                printf("Podaj index kolumny \n");
                                scanf("%d",&y);
                                print_Value_From_Index(x,y);
                        break;
                        case 5:
                                printf("Podaj index wiersza \n");
                                scanf("%d",&x);
                                printf("Podaj Wartosc \n");
                                scanf("%d",&a);
                                fill_Row(x,a);
                        break;
                        case 6:
                                printf("Podaj index kolumny \n");
                                scanf("%d",&y);
                                printf("Podaj Wartosc \n");
                                scanf("%d",&a);
                                fill_Column(y,a);
                        break;
                        case 7:
                                printf("Podaj index wiersza \n");
                                scanf("%d",&x);
                                printf("Wynik sumowania wierszu o indexie %d  wynik = %d \n",x,sum_Value_Array(x,1,0));
                        break;
                        case 8:
                                printf("Podaj index kolumny \n");
                                scanf("%d",&y);
                                printf("Wynik sumowania kolumny  o indexie %d  wynik = %d \n",y,sum_Value_Array(1,y,1));
                        break;
                        case 9:
                                printf("Wynik sumowania tablicy   wynik = %d \n",sum_Value_Array(1,1,2));
                        break;
                        case 10:
                                sum_Diagonals();
                        break;
                        case 11:
                                printf("Podaj index wiersza \n");
                                scanf("%d",&x);
                                printf("Podaj index kolumny \n");
                                scanf("%d",&y);
                                print_array(x,y);
                        break;
                       
                }
        }while(select!=0); 
    
        return 0;
}